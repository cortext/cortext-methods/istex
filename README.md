# Istex

A parser of Istex exports for Cortext Manager.

## Istex Format
Some info on Istex exports:
- a tar file, gzip compressed
- two top-level files: file.corpus, manifest.json
- many top-level directories, each corresponds to a document in the corpus
- document directories are named as some kind of hash: [A-F0-9]+
- document directories contain:
  - {hash}.{extension}
  - a pdf and a xml, originals from the editor

## Cortext Format
Logical format of the Cortext database:

Mapping0[Text0 -> Tuple0[Sequence0[Sequence1[Sequence2[Sequence3[[Text1, Data0]]]]], Type0]]

Semantics:
- Mapping0: the database
- Text0: table name
- Sequence0: first index (id)
- Sequence1: second index (rank)
- Sequence2: third index (parserank)
- Sequence3: implicit index of insertion order (sequence)
- Text1: source file for the data point
- Data0: value of the data point
- Type0: type of all data points in the table

Sequence{0,1,2} are implemented as integer-valued columns in the table.

Text1 is of type:

str

Type0 is restricted to:

Union[str, int, float]

Information about a single document thus fixes a single index at Sequence0:

Mapping0[Text0 -> Tuple0[Sequence1[Sequence2[Sequence3[[Text1, Any0]]]], Type0]]

If we drop the table and type:

Sequence1[Sequence2[Sequence3[[Text1, Any0]]]]

If we drop the source file:

Sequence1[Sequence2[Sequence3[Any0]]]
