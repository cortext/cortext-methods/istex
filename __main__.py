#!/usr/bin/env python

import sys
from cortextlib import Job
from istex import Method


def main():
    parameters_file = sys.argv[1]
    job = Job(ctm_params=parameters_file)
    with Method(job) as method:
        method.run()


if __name__ == "__main__":
    main()
