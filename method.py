from contextlib import AbstractContextManager
from functools import wraps
import json

from cortextlib import CortextMethod


class Istex(CortextMethod):
    """
    Parser method for Istex exports.
    Always extracts as much info as possible from the exported archive, since
    someone wanting less info should simply request a less complete export from Istex.
    """

    def __init__(self, job):
        """
        Initialize super and treat parameters.
        """
        super().__init__(job)
        # Add parameter processing here
        self.params.clear()

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Exit base class and run cleanup logic, if any.
        """
        super().__exit__(exc_type, exc_value, traceback)

    def main(self):
        """
        Do not pass `self.job` to calls, pass attributes so we can track their usage from
        here.
        """
        from . import parser, storage

        log = self.job.log

        log.info("Creating database")
        db_path = self.job.dir / (self.job.db.name + ".db")
        db_con = storage.create_database(db_path, log)

        try:
            log.info("Processing documents")
            with HookProgressIstex(self.job.progress, parser) as hook:
                parser.extract_and_load(self.job.db, db_con, self.job.dir, log)
                log.info(
                    f"Processed {hook.num_documents} documents,"
                    f" with text souce cleaned for {hook.num_cleaned}."
                )

            log.info("Registering database")
            storage.register(
                db_path, db_con, self.job.db.name, self.job.db_meta_update, log
            )
        finally:
            log.info("Closing database connection")
            db_con.close()

        log.info("Finished!")


class HookProgressIstex(AbstractContextManager):
    def __init__(self, progress, parser):
        self.num_documents = None
        self.num_cleaned = 0

        # Hook on arcPath()
        self.arcPath = parser.arcPath

        @wraps(self.arcPath)
        def arcPath_wrapper(archive_path, *args, **kwargs):
            archive = self.arcPath(archive_path, *args, **kwargs)
            self.num_documents = len([x for x in archive.iterdir() if x.is_dir()])
            progress.set_total(self.num_documents)
            return archive

        parser.arcPath = arcPath_wrapper

        # Hook on parsed_document()
        self.parsed_document = parser.DocumentParser.parsed_document

        @wraps(self.parsed_document)
        def parsed_document_wrapper(*args, **kwargs):
            data, _ = ret = self.parsed_document(*args, **kwargs)
            progress.advance()
            if json.loads(data["body_text_source"][0][0])["file"] == "cleaned":
                self.num_cleaned += 1
            return ret

        parser.DocumentParser.parsed_document = parsed_document_wrapper

        self.parser = parser

    def __exit__(self, exc_type, exc_value, traceback):
        # Unhook
        self.parser.arcPath = self.arcPath
        self.parser.DocumentParser.parsed_document = self.parsed_document
