#!/usr/bin/env python

import io
from pathlib import Path
import posixpath
import tarfile
import zipfile


class TarPath(object):
    """
    Navigate a tarfile using an API similar to `pathlib.Path`.
    """

    def __init__(self, root, at=None):
        if isinstance(root, tarfile.TarFile):
            self.root = root
        elif isinstance(root, TarPath):
            self.root = root.root
            at = root.at if at is None else at
        else:
            self.root = tarfile.open(root)
        self.at = "" if at == "." or not at else posixpath.normpath(at)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.root.fileobj.name!r}, {self.at!r})"

    def __str__(self):
        return posixpath.join(self.root.fileobj.name, self.at)

    def __truediv__(self, name):
        return TarPath(self.root, posixpath.normpath(posixpath.join(self.at, name)))

    @property
    def info(self):
        """
        The TarInfo for the name in the archive, or `None` for the archive root.
        """
        return self.root.getmember(self.at) if self.at else None

    @property
    def name(self):
        """
        The final name component, or an empty string for the archive root.
        """
        return posixpath.basename(self.at.rstrip("/"))

    # Borrow name-related methods from `pathlib.Path`
    suffix = Path.suffix
    suffixes = Path.suffixes
    stem = Path.stem

    def iterdir(self):
        """
        Iterate over the files in a directory.
        """
        if not self.is_dir():
            raise ValueError("Not a directory.")
        children = set()
        for arcname in self.root.getnames():
            if arcname.startswith(self.at):
                name, *_ = arcname[len(self.at) :].lstrip("/").split("/", 1)
                if name and name not in children:
                    children.add(name)
                    yield self / name

    def is_dir(self):
        """
        Whether this path is a directory.
        """
        try:
            return self.info.isdir() if self.at else True
        except KeyError:
            for arcname in self.root.getnames():
                if arcname.startswith(self.at):
                    if arcname[len(self.at) : len(self.at) + 1] == "/":
                        return True
            return False

    def open(self, mode="r", encoding=None, errors=None):
        """
        Open the file, extracting from the TarFile, and return:
        - in bytes mode, an `io.BufferedReader`
        - in text mode, an `io.TextIOWrapper`
        Currenly does not support writing.
        """
        if not self.at:
            raise ValueError("Can't open archive root.")
        if mode not in ("r", "rb"):
            raise NotImplementedError("Can only open for reading.")
        buffered_reader = self.root.extractfile(self.at)
        if mode == "rb":
            return buffered_reader
        if mode == "r":
            return io.TextIOWrapper(buffered_reader, encoding=encoding, errors=errors)

    def iterarchive(self):
        return (TarPath(self.root, info.name) for info in self.root)

    # Borrow open-related methods from `pathlib.Path`
    read_bytes = Path.read_bytes
    read_text = Path.read_text


class ZipPath(zipfile.Path):
    suffix = Path.suffix
    suffixes = Path.suffixes
    stem = Path.stem

    def iterarchive(self):
        return (ZipPath(self.root, name) for name in self.root.namelist())

    def _next(self, at):
        return ZipPath(self.root, at)


def arcPath(root, at=None):
    if isinstance(root, (tarfile.TarFile, TarPath)):
        return TarPath(root, at)
    if isinstance(root, zipfile.ZipFile):
        return ZipPath(root, "" if at is None else at)
    if isinstance(root, ZipPath):
        return ZipPath(root.root, root.at if at is None else at)
    if str(root).endswith(".zip"):
        return ZipPath(root, "" if at is None else at)
    return TarPath(root, at)
