import pandas as pd

TERMS_LIST_COLUMNS = [
    "Stem",
    "Main form",
    "Forms",
    "Frequency",
    "Specificity",
    "Documents",
]

TEEFT_TERMS_LIST_PATH = "teeft_terms_list.tsv"


def teeft_tsv_to_cortext_terms_list(teeft_path):
    """
    Aggregating the frequency and specificity values is heuristic at best, since data about a term
    is only showed for a document if its specificity is above a certain threshold (seemingly 0.001).
    For specificities, we sum the specificities of a term for all documents where it appears and
    divide by the number of documents containing Teeft information.
    We also filter the terms by replaying the specificity threshold with the aggregate values.
    See <https://gitlab.com/cortext/cortext-methods/istex/-/issues/8#note_1304258470>.
    """
    df = pd.read_csv(teeft_path)

    specificity_threshold = 0.001
    total_docs = len(df["Istex ID"].unique())

    df = (
        df.groupby("Term")
        .agg({"Frequency": "sum", "Specificity": "sum", "Istex ID": "count"})
        .reset_index()
    )
    df["Specificity"] /= total_docs
    df = df[df["Specificity"].ge(specificity_threshold)]
    df = df.sort_values("Specificity", ascending=False)
    df = df[["Term", "Term", *df.columns]]
    df.columns = TERMS_LIST_COLUMNS

    df.to_csv(teeft_path.parent / TEEFT_TERMS_LIST_PATH, sep="\t", index=False)
