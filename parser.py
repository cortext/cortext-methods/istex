import csv
from functools import wraps
from io import StringIO
import json
import re

import lxml.etree
import pycountry

from . import conversions
from .storage import Database, write_to_fs
from .tarpath import arcPath


class NullLogger:
    @staticmethod
    def null_log(x):
        pass

    def __getattribute__(self, x):
        return self.null_log


class IstexDataError(Exception):
    """
    Use for known *data errors* in the files produced by Istex.
    (Format errors will throw an XMLSyntaxError)

    This Exception sould only be used for debugging purposes,
    at least until Cortext has a system in place to automatically:
    - inform users that they're consuming bad data.
    - inform Istex that they're producing bad data.
    """

    pass


def log_parse_errors(errors, log):
    parse_errors = {
        x: y
        for x, y in errors.items()
        if isinstance(y, (lxml.etree.XMLSyntaxError, IstexDataError))
    }
    if not parse_errors:
        return
    messages = [
        f"The database created is INCOMPLETE. The corpus contains {len(parse_errors)} malformed"
        " documents that could not be included.",
        "",
        "The Istex `id`s of the malformed documents are available in the job log. Click the job"
        " status flag to access it.",
        "",
        "You should contact Istex with the `id`s of those documents to fix the issue:"
        " <https://www.istex.fr/contact/>.",
    ]
    log.info("------------------------------------------------------------------------")
    log.warning("\n".join(messages))
    messages = [
        "------------------------------------------------------------------------",
        f"The Istex `id`s and errors of the {len(parse_errors)} malformed documents are:",
    ]
    for doc, error in parse_errors.items():
        messages.append(f"{doc} ({type(error).__name__}: {error})")
    log.info("\n".join(messages))


def extract_and_load(archive_path, db_con, result_path, log=NullLogger()):
    """
    Extracts data from an Istex archive and stores it in a database.
    Treat one document at a time so we can treat huge corpora.
    """
    errors = {}
    archive = arcPath(archive_path)
    database = Database(db_con)
    write_to_fs(DocumentParser.init_teeft_tei_xml(), result_path)
    for id, document_dir in enumerate(x for x in archive.iterdir() if x.is_dir()):
        try:
            data, files = DocumentParser.parsed_document(document_dir, log)
            database.store_document(
                f"{archive_path.name}/{document_dir.name}", id, data
            )
            write_to_fs(files, result_path)
        except (lxml.etree.XMLSyntaxError, IstexDataError) as error:
            errors[document_dir.name] = error
    log_parse_errors(errors, log)
    try:
        conversions.teeft_tsv_to_cortext_terms_list(
            result_path / DocumentParser.teeft_out_fname
        )
    except (Exception) as e:
        log.warning(f"Consolidating Teeft terms list failed: {e}")


def extract_and_return(archive_path, log=NullLogger()):
    """
    Extracts data from an Istex archive and returns a dictionary.
    """
    res = {}
    archive = arcPath(archive_path)
    for document_dir in (x for x in archive.iterdir() if x.is_dir()):
        log.debug(document_dir)
        data, files = DocumentParser.parsed_document(document_dir, log)
        res[document_dir.name] = dict(data=data, files=files)
    return res


def xml_parser(parser_function):
    """
    Decorator for XML parser functions that parses the XML file,
    passing to the function only the element tree and namespace map.
    """

    @wraps(parser_function)
    def wrapper(*args, file):
        # Disable id collection because Istex produces duplicate IDs
        # Enable recovery mode because Istex produces non-escaped tag-like text
        # /me double_facepalms
        etree = lxml.etree.parse(
            file, lxml.etree.XMLParser(collect_ids=False, recover=True)
        )
        nsmap = etree.getroot().nsmap
        # Add implicit xml namespace in nsmap for, e.g., xml:lang
        nsmap["xml"] = "http://www.w3.org/XML/1998/namespace"
        return parser_function(*args, etree, nsmap)

    return wrapper


class DocumentParser:
    """
    Translates the different files into the Cortext DB format (see README).
    We end up losing some information because the DB format is limiting.
    """

    teeft_out_fname = "documents_teeft_terms.csv"

    @classmethod
    def parsed_document(cls, document_path, log=NullLogger()):
        data, files = {}, {}
        # Triage files
        file_paths = {".xml": [], ".txt": [], ".cleaned": [], ".json": []}
        for file_path in document_path.iterdir():
            if purestem(file_path) == purestem(document_path):
                if file_path.suffix in file_paths:
                    file_paths[file_path.suffix].append(file_path)

        # Treat XML files
        for file_path in file_paths[".xml"]:
            file_data, file_files = cls.parsed_contents(file_path, log)
            data.update(file_data)
            files.update(file_files)

        # Get text body from '.txt' file in case none was found as TEI/XML
        if not data.get("body_paragraphs", None):
            for file_path in file_paths[".txt"]:
                if file_path.name == f"{purestem(document_path)}.txt":
                    file_data, _ = cls.parse_txt(file_path)
                    data.update(file_data)

        # Replace 'body_text' with '.cleaned' file if it is present
        for file_path in file_paths[".cleaned"]:
            if file_path.name == f"{purestem(document_path)}.cleaned":
                file_data, _ = cls.parse_cleaned(file_path)
                data.update(file_data)

        # Treat JSON file
        if len(file_paths[".json"]) == 1:
            """Don't get anything from JSON for now, as TEI seems better."""
            pass

        # FIXME Remove body_paragraphs because of
        # https://gitlab.com/cortext/cortext-methods/istex/-/issues/8#note_1294026390
        for key in ("body_paragraphs", "body_paragraphs_source"):
            del data[key]

        return data, files

    @classmethod
    def parsed_contents(cls, file_path, log):
        """
        Parser functions should be static methods taking a file object and returning two dicts:
        - data: {field: [[value]]} entries for the database.
        - files:{name: contents} files to be created.
        """
        parser_function_name = f"parse{''.join(file_path.suffixes).replace('.', '_')}"
        try:
            parser_function = getattr(cls, parser_function_name)
        except AttributeError:
            return {}, {}
        with file_path.open() as file:
            log.debug(f"  {file_path}")
            return parser_function(file=file)

    @staticmethod
    def parse_txt(file):
        """
        UNUSED currently as body seems always present in '.tei.xml'
        Consolidate likely paragraphs as Istex breaks them.
        """
        body_paragraphs = unbreak_paragraphs_then_split(file.read())
        body_text = "\n\n".join(body_paragraphs)
        return {
            "body_paragraphs": [body_paragraphs],
            "body_paragraphs_source": [[json.dumps({"file": "txt", "format": "txt"})]],
            "body_text": [[body_text]],
            "body_text_source": [[json.dumps({"file": "txt", "format": "txt"})]],
        }, {}

    @staticmethod
    def parse_cleaned(file):
        """Simply load file contents into data"""
        return {
            "body_text": [[file.read_text()]],
            "body_text_source": [[json.dumps({"file": "cleaned", "format": "txt"})]],
        }, {}

    @staticmethod
    def parse_json(file):
        """Don't get anything from JSON for now, as TEI seems better."""
        # raw = json.load(file)
        # data = {}
        return {}, {}

    @staticmethod
    @xml_parser
    def parse_tei_xml(etree, nsmap):
        """Main metadata file
        TODO: parse bibliography?
        """
        data, files = {}, {}

        data.update(TEIParser.basic(etree, nsmap))
        data.update(TEIParser.venue(etree, nsmap))
        data.update(TEIParser.body(etree, nsmap))
        data.update(TEIParser.dates(etree, nsmap))
        data.update(TEIParser.ids(etree, nsmap))
        data.update(TEIParser.authors(etree, nsmap))

        return data, files

    @staticmethod
    @xml_parser
    def parse_mods_xml(etree, nsmap):
        return {}, {}

    @staticmethod
    @xml_parser
    def parse_nb_tei_xml(etree, nsmap):
        return {}, {}

    @staticmethod
    @xml_parser
    def parse_refbibs_tei_xml(etree, nsmap):
        return {}, {}

    @classmethod
    @xml_parser
    def parse_teeft_tei_xml(cls, etree, nsmap):
        contents = StringIO("excel-tab", newline="")
        writerow = csv.writer(contents).writerow

        (istex_id,) = epath_text(
            etree,
            nsmap,
            "/ns1:standOff/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/idno[@type='ISTEX']",
        )
        for e_term in etree.iterfind(
            "ns1:standOff/ns1:listAnnotation/annotationBlock/keywords/term", nsmap
        ):
            (term,) = epath_text(e_term, nsmap, "term")
            frequency = e_term.findall('fs/f[@name="frequency"]/numeric', nsmap)[
                0
            ].attrib["value"]
            specificity = e_term.findall('fs/f[@name="specificity"]/numeric', nsmap)[
                0
            ].attrib["value"]
            writerow([istex_id, term, frequency, specificity])

        return {}, {cls.teeft_out_fname: contents.getvalue()}

    @classmethod
    def init_teeft_tei_xml(cls):
        contents = StringIO("excel-tab", newline="")
        writerow = csv.writer(contents).writerow
        writerow(["Istex ID", "Term", "Frequency", "Specificity"])
        return {cls.teeft_out_fname: contents.getvalue()}


class TEIParser(object):
    """
    Functions to parse TEI XML.
    All public methods should be parsing methods, i.e.:
    ```
    def public_method(etree, nsmap):
        ...
        return {"field": [[value, ...]]}
    ```
    """

    @staticmethod
    def basic(etree, nsmap):
        data = {}

        language = None
        e_language = next(
            etree.iterfind("/teiHeader/profileDesc/langUsage/language", nsmap), None
        )
        if e_language is not None:
            language = e_language.attrib["ident"].lower()
            data["language"] = [[language]]

        iter_abstract = etree.iterfind("/teiHeader/profileDesc/abstract", nsmap)
        abstracts = []
        for e_abstract in iter_abstract:
            if (
                e_abstract.attrib.get(f"{{{nsmap['xml']}}}lang", language).lower()
                == language
            ) and e_abstract.attrib.get("style", None) not in ("graphical", "short"):
                lxml.etree.strip_elements(
                    e_abstract, f"{{{nsmap.get(None, '')}}}head", with_tail=False
                )
                if abstract := epath_text(e_abstract, nsmap, ".", "\n"):
                    abstracts.append(
                        re.sub(
                            r"^(?:(?:[Aa]bstract|[Ss]ummary|[Rr]ésumé):\s*)+",
                            "",
                            abstract,
                        )
                    )
        if abstracts:
            data["abstract"] = [abstracts]

        title = next(
            (
                epath_text(e_title, nsmap, ".", " ")
                for e_title in etree.iterfind(
                    "/teiHeader/fileDesc/titleStmt/title", nsmap
                )
                if e_title.attrib.get("type", "main") == "main"
                and e_title.attrib.get("xml:lang", language) == language
            ),
            None,
        )
        if title is not None:
            data["title"] = [[title]]

        return data

    @staticmethod
    def venue(etree, nsmap):
        data = {}

        journal = next(
            (
                epath_text(e_journal, nsmap, ".", " ")
                for e_journal in etree.iterfind(
                    "/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/title", nsmap
                )
                if e_journal.attrib.get("type", "main") == "main"
                and e_journal.attrib.get("level", None) == "j"
            ),
            None,
        )
        if journal is not None:
            data["journal"] = [[journal]]

        monographs = [
            next(
                (
                    epath_text(e_title, nsmap, ".", " ")
                    for e_title in e_mono.iterfind("title", nsmap)
                    if e_title.attrib.get("type", "main") == "main"
                    and e_title.attrib.get("level", None) == "m"
                ),
                None,
            )
            for e_mono in etree.iterfind(
                "/teiHeader/fileDesc/sourceDesc/biblStruct/monogr", nsmap
            )
        ]
        if monographs := [x for x in monographs if x is not None]:
            data["monographs"] = [monographs]

        return data

    @staticmethod
    def body(etree, nsmap):
        epath = "/text/body"
        paragraphs = epath_text(etree, nsmap, epath)
        # some TEI provide an unstructured body as <div><p>text</p></div>
        not_structured = all(
            [e_y.tag for e_y in e_x.iterdescendants()]
            == [f"{{{nsmap.get(None, '')}}}{z}" for z in ("div", "p")]
            for e_x in etree.iterfind(epath, nsmap)
        )
        if not_structured:
            paragraphs = [
                y for x in paragraphs for y in unbreak_paragraphs_then_split(x)
            ]
        body_text = "\n\n".join(paragraphs)
        bps = json.dumps({"file": "tei", "format": "txt" if not_structured else "xml"})
        bts = json.dumps({"file": "tei", "format": "txt" if not_structured else "xml"})
        return {
            "body_paragraphs": [paragraphs],
            "body_paragraphs_source": [[bps]],
            "body_text": [[body_text]],
            "body_text_source": [[bts]],
        }

    @staticmethod
    def ids(etree, nsmap):
        dois = epath_text(
            etree,
            nsmap,
            "/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/idno[@type='DOI']",
        )
        istex_ids = epath_text(
            etree,
            nsmap,
            "/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/idno[@type='istex']",
        )
        return {"doi": [dois], "istex": [istex_ids]}

    @staticmethod
    def dates(etree, nsmap):
        data = {}
        iter_date = etree.iterfind("/teiHeader/fileDesc/publicationStmt/date", nsmap)
        dates, date_pub = [], None
        for e_date in iter_date:
            try:
                (date,) = epath_text(e_date, nsmap, ".")
            except ValueError:
                date = e_date.attrib.get("when", None)
            if date is not None:
                dates.append(date)
                if (date_pub is None) and (
                    e_date.attrib.get("type", None) == "published"
                ):
                    date_pub = date
        if dates:
            data["all_dates"] = [dates]
            if date_pub is None:
                date_pub = dates[0]
        if date_pub is not None:
            data["ISIpubdate"] = [[int(re.search(r"\d{4}", date_pub).group())]]

        return data

    @classmethod
    def authors(cls, etree, nsmap):
        data = {}
        author_fields = (
            "authors_names",
            "authors_fullname",
            "affiliations_name",
            "affiliations_address",
            "affiliations_country",
            "affiliations",
        )
        # authors loop (rank)
        for field in author_fields:
            data[field] = []
        for e_author in etree.iterfind(
            "/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/author", nsmap
        ):
            author_names = [
                epath_text(e_author, nsmap, "persName/forename", " "),
                epath_text(e_author, nsmap, "persName/surname", " "),
                epath_text(e_author, nsmap, "persName/genName", " "),
                epath_text(e_author, nsmap, "persName/roleName", " "),
            ]
            if any(author_names):
                data["authors_names"].append(author_names)
                data["authors_fullname"].append([" ".join(author_names[:-1])])
            elif text := epath_text(e_author, nsmap, "persName", " "):
                data["authors_fullname"].append([text])
            elif text := epath_text(e_author, nsmap, "name", " "):
                data["authors_fullname"].append([text])

            cls._set_affiliations(data, e_author, nsmap, author_fields)

        return data

    @staticmethod
    def _set_affiliations(data, e_author, nsmap, author_fields):
        # affiliations loop (parserank)
        for field in author_fields:
            if field.startswith("affiliations"):
                data[field].append([])
        for e_affiliation in e_author.iterfind("affiliation", nsmap):
            if text := epath_text(e_affiliation, nsmap, "orgName", "; "):
                data["affiliations_name"][-1].append(text)
            if text := epath_text(e_affiliation, nsmap, "address", ", "):
                data["affiliations_address"][-1].append(text)
            e_country = next(e_affiliation.iterfind("address/country", nsmap), None)
            if e_country is not None:
                if country := (
                    e_country.attrib.get("key", None)
                    or e_country.text
                    and (
                        getattr(
                            pycountry.countries.get(name=e_country.text),
                            "alpha_2",
                            None,
                        )
                        or getattr(
                            pycountry.countries.get(official_name=e_country.text),
                            "alpha_2",
                            None,
                        )
                    )
                ):
                    data["affiliations_country"][-1].append(country.upper())
            # also get full affiliation text because some docs are messy
            if text := epath_text(e_affiliation, nsmap, ".", "; "):
                data["affiliations"][-1].append(text)


def epath_text(e_root, nsmap, epath, separator=None):
    """
    Returns all text under all `epath` of element `e_root`.
    Removes no-text tags (e.g., <ref>, <hi>) in paragraph elements to avoid splits.
    Returns a list with the texts for every element in hierarchy order.
    If passed a `separator`, use it to join the list into a string.
    """
    text = []
    for tag_element in e_root.iterfind(epath, nsmap):
        strip_tags = set([x.tag for x in tag_element.iterfind(".//p/*", nsmap)])
        # In case we ever want to add a space where we removed the tag...
        # for e_strip in tag_element.iter(strip_tags):
        #     if e_strip.text is None:
        #         e_strip.text = " "
        lxml.etree.strip_tags(tag_element, strip_tags)
        text.extend(_text for x in tag_element.itertext() if (_text := x.strip()))
    return text if separator is None else separator.join(text)


def purestem(path):
    """Returns a stem stripped of all suffixes."""
    return path.name[: len(path.name) - len("".join(path.suffixes))]


def unbreak_paragraphs_then_split(text):
    """
    Reconstitutes paragraphs in text with 1 line break inside them and >2 outside.
    TODO: remove identical paragraphs (headers, footers) and number-only paragraphs (page numbers)
    before unbreaking.
    """
    return [
        _text
        for x in re.sub(r"(?<![\.\n])\n(?![\n])", " ", text).split("\n")
        if (_text := x.strip())
    ]
