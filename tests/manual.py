#!/usr/bin/env python

"""
%load_ext autoreload
%autoreload 2
%aimport
"""

from pathlib import Path
import shutil

import cortextlib
import cortext_istex

corpus_file = Path("istex-subset-2021-10-21.zip")
corpus_file = Path("istex-subset-2021-09-23.tar.gz")
corpus_file = Path("istex-subset-2022-09-14.tar.gz")
corpus_file = Path("istex-subset-2022-09-26.tar.gz")
corpus_file = Path("istex-subset-2022-09-27.zip")

corpus_result_path = (corpus_file.parent / corpus_file.stem)
corpus_result_path.mkdir(exist_ok=True)
cortext_istex.Method(
    cortextlib.Job(
        dict(
            script_path="istex",
            result_path=corpus_file.stem,
            corpus_name=str(corpus_file.with_suffix(".test")),
            corpus_file=corpus_file,
        )
    )
).run()
shutil.rmtree(corpus_result_path)
